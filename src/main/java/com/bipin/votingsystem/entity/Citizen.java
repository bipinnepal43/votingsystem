package com.bipin.votingsystem.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Entity
public class Citizen implements Serializable {
    @Id
    private long id;
    private String name;

}
